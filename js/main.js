(async () => {
  await fetchDataAndRenderCards();
})()

/**
 *
 * @param{string} url
 * @returns {Promise<Object[]>}
 */
async function fetchEntities(url) {
  let response = await fetch(url).then(res => res.json());

  let results = response?.results;
  if (!results) return [];
  let result = results;
  while (response.next) {
    response = await fetch(response.next).then(res => res.json());
    results = response?.results;
    if (results) {
      result.push(...results);
    }
  }
  return result;
}

/**
 *
 * @param {[Object]} planets
 * @param {number} pageNumber
 * @param {number} itemsPerPage
 */
function renderPagination(planets, pageNumber, itemsPerPage = 10) {
  const paginationList = document.querySelector('.cards_pagination');
  const itemsCount = planets.length;
  let liFromLink = 'page-item', liToLink = 'page-item';
  if (pageNumber === 1) {
    liFromLink += ' disabled';
  } else if (pageNumber === Math.round(itemsCount / itemsPerPage)) {
    liToLink += ' disabled';
  }
  const toNumber = pageNumber * itemsPerPage;
  const fromNumber = toNumber - itemsPerPage + 1;
  paginationList.innerHTML = `
  <li class="${liFromLink}">
    <a class="page-link page-prev" href="#" aria-label="Previous">
      <span aria-hidden="true">&laquo;</span>
    </a>
  </li>
  <li class="page-item">${fromNumber}-${toNumber}/${itemsCount}</li>
  <li class="${liToLink}">
    <a class="page-link page-next" href="#" aria-label="Next">
      <span aria-hidden="true">&raquo;</span>
    </a>
  </li>`
  paginationList.querySelectorAll('.page-link').forEach(link => {
    link.addEventListener('click', async e => {
      e.preventDefault();
      if (e.currentTarget.classList.contains('page-next')) {
        pageNumber++;
      } else if (e.currentTarget.classList.contains('page-prev')) {
        pageNumber--;
      } else {
        return
      }
      await renderCards(planets, pageNumber, itemsPerPage);
    }, {once: true});
  });
}

let PLANETS;

async function fetchDataAndRenderCards() {
  PLANETS = await fetchEntities('https://swapi.dev/api/planets/');
  await renderCards(PLANETS);
}

async function renderCards(planets, pageNumber = 1, cardsPerPage = 10) {
  const cardsContainer = document.querySelector('.cards_container');
  cardsContainer.innerHTML = '';
  const numTo = pageNumber * cardsPerPage;
  const numFrom = numTo - cardsPerPage;
  planets.slice(numFrom, numTo).forEach(planet => {
    const planetCard = renderPlanetAsCard(planet);
    cardsContainer.appendChild(planetCard);
  })
  renderPagination(planets, pageNumber, cardsPerPage)
}

function renderPlanetAsCard(planet) {
  const planetCard = document.createElement('div');
  planetCard.classList.add('card', 'planet');
  const planetInfoHTML = `
          <h5 class="card-title card-header planet__title">Planet <span class="planet__name">'${planet.name}'</span></h5>
          <dl class="row planet__definition-list">
            <dt class="col-6 planet__diameter">Diameter:</dt>
            <dd class="col-6 planet__definition-data">${planet.diameter}</dd>
            <dt class="col-6 planet__population">Population:</dt>
            <dd class="col-6 planet__definition-data">${planet.population}</dd>
            <dt class="col-6 planet__gravity">Gravity:</dt>
            <dd class="col-6 planet__definition-data">${planet.gravity}</dd>
            <dt class="col-6 planet__terrain">Terrain:</dt>
            <dd class="col-6 planet__definition-data">${planet.terrain}</dd>
            <dt class="col-6 planet__climate">Climate:</dt>
            <dd class="col-6 planet__definition-data">${planet.climate}</dd>
        </dl>`
  planetCard.innerHTML = planetInfoHTML;

  const cardButton = document.createElement('button');
  cardButton.setAttribute('type', 'button');
  cardButton.setAttribute('data-bs-toggle', 'modal');
  cardButton.setAttribute('data-bs-target', '#planetDetailModal');
  cardButton.classList.add('btn', 'btn-primary', 'planet__details-button');
  cardButton.innerText = 'Details';
  planetCard.appendChild(cardButton);

  cardButton.addEventListener('click', () => {
    renderModal(planet.name, planet.films, planet.residents, planetInfoHTML);
  });
  return planetCard;
}

/**
 *
 * @param {string}    planetTitle
 * @param {[string]}  filmUrls
 * @param {[string]}  residentUrls
 * @param {string}    planetInfoHTML
 * @returns {Promise<void>}
 */
async function renderModal(planetTitle, filmUrls, residentUrls, planetInfoHTML) {
  const filmPromiseArray = filmUrls.map(filmUrl => {
    return getEntityJson(filmUrl);
  })
  const residentsPromiseArray = residentUrls.map(residentUrl => {
    return getEntityJson(residentUrl);
  })

  const modalBody = document.querySelector('.modal-body');
  const modalTitle = document.querySelector('.planet-detail__title');
  modalTitle.textContent = planetTitle;

  modalBody.innerHTML = planetInfoHTML;
  modalBody.innerHTML += `
  <div class="planet-detail__spinner">
    <div class="d-flex align-items-center">
      <strong>Loading...</strong>
      <div class="spinner-border ms-auto" role="status" aria-hidden="true"></div>
    </div>
  </div>`

  const spinner = modalBody.querySelector('.planet-detail__spinner');

  const films = await Promise.all(filmPromiseArray);
  const filmsTableElement = createFilmsTableElement(films);
  const residents = await Promise.all(residentsPromiseArray);
  const residentsTableElement = await createResidentsTableElement(residents);

  spinner.style.display = 'none';

  modalBody.appendChild(filmsTableElement);
  modalBody.appendChild(residentsTableElement);

  console.log(films);
  console.log(residents);
}

/**
 *
 * @param {[Object]} films
 * @returns {HTMLTableElement}
 */
function createFilmsTableElement(films) {
  const table = document.createElement('table');
  table.classList.add('table', 'table-striped', 'caption-top');
  table.innerHTML = `
              <caption>List of films</caption>
              <thead>
              <tr>
                <th scope="col">Episode Id</th>
                <th scope="col">Title</th>
                <th scope="col">Release date</th>
              </tr>
              </thead>`;
  const tBody = document.createElement('tbody');
  const filmDataRows = films.map(film => {
    const tr = document.createElement('tr');
    tr.innerHTML = `
    <th scope="row">${film.episode_id}</th>
    <td>${film.title}</td>
    <td>${film.release_date}</td>
    `;
    return tr;
  });
  tBody.append(...filmDataRows);
  table.appendChild(tBody);
  return table;
}

/**
 *
 * @param {[Object]} residents
 * @returns {HTMLTableElement}
 */
async function createResidentsTableElement(residents) {
  const table = document.createElement('table');
  table.classList.add('table', 'table-striped', 'caption-top');
  table.innerHTML = `
              <caption>List of residents</caption>
              <thead>
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Gender</th>
                <th scope="col">Birth year</th>
                <th scope="col">Home world name</th>
              </tr>
              </thead>`;
  const tBody = document.createElement('tbody');
  const residentDataRowsPromise = residents.map(async resident => {
    const planet = await getEntityJson(resident.homeworld);
    const tr = document.createElement('tr');
    tr.innerHTML = `
    <th scope="row">${resident.name}</th>
    <td>${resident.gender}</td>
    <td>${resident.birth_year}</td>
    <td>${planet.name}</td>
    `;
    return tr;
  });
  const residentDataRows = await Promise.all(residentDataRowsPromise);
  tBody.append(...residentDataRows);
  table.appendChild(tBody);
  return table;
}


/**
 *
 * @param {function|null} entitiesFindPredicate
 * @param {string} url
 * @returns {Promise<Object>}
 */
async function getEntityJson(url, entitiesFindPredicate = null) {
  let response = await fetch(url).then(res => res.json());

  if (!entitiesFindPredicate) return response;

  let entity = response?.results?.find(entitiesFindPredicate);
  while (!entity && response.next) {
    response = await fetch(response.next).then(res => res.json());
    entity = response.results?.find(entitiesFindPredicate);
  }
  return entity;
}